import React, { useState } from 'react';
import Swal from 'sweetalert2';
import 'bootstrap/dist/css/bootstrap.min.css';
import { IconContext } from 'react-icons';
import { BiLogInCircle, BiLogOutCircle } from 'react-icons/bi'; // Import Bootstrap Icons


const ClockInOut = () => {
  const [clockedIn, setClockedIn] = useState(false);
  const [timeLogs, setTimeLogs] = useState([]);

  const handleClockIn = () => {
    Swal.fire({
      title: 'Are you sure you want to clock in?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, clock in!',
      cancelButtonText: 'No, cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        const currentDateTime = new Date();
        setTimeLogs([...timeLogs, { date: currentDateTime.toLocaleDateString(), clockIn: currentDateTime }]);
        setClockedIn(true);
        Swal.fire('Clocked in!', 'You have successfully clocked in.', 'success');
      }
    });
  };

  const handleClockOut = () => {
    Swal.fire({
      title: 'Are you sure you want to clock out?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, clock out!',
      cancelButtonText: 'No, cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        const currentDateTime = new Date();
        setTimeLogs(timeLogs.map((log, index) =>
          index === timeLogs.length - 1 ? { ...log, clockOut: currentDateTime } : log
        ));
        setClockedIn(false);
        Swal.fire('Clocked out!', 'You have successfully clocked out.', 'success');
      }
    });
  };

  const calculateTotalTime = (log) => {
    if (!log.clockIn || !log.clockOut) return '-';
    const difference = (log.clockOut - log.clockIn) / 1000 / 60; // difference in minutes
    const hours = Math.floor(difference / 60);
    const minutes = Math.round(difference % 60);
    return `${hours} hours ${minutes} minutes`;
  };

  return (
    <div className="container">
      <h2 className="pt-4"> Hello, employeeID </h2>
      <p> <h5>Project Name</h5>  Clock In / Clock Out </p>
      

      {!clockedIn ? (
        <button className="btn btn-primary" onClick={handleClockIn}>
          <IconContext.Provider value={{ className: 'icon' }}>
             Clock In <BiLogInCircle />
          </IconContext.Provider>
        </button>
      ) : (
        <button className="btn btn-danger" onClick={handleClockOut}>
          <IconContext.Provider value={{ className: 'icon' }}>
           Clock Out   <BiLogOutCircle />
          </IconContext.Provider>
        </button>
      )}

      <div className="time-logs mt-4">
        <h3>Time Logs</h3>
        <table className="table table-bordered rounded-table">
          <thead>
            <tr>
              <th>Date</th>
              <th>Clock In</th>
              <th>Clock Out</th>
              <th>Total Time</th>
            </tr>
          </thead>
          <tbody>
            {timeLogs.map((log, index) => (
              <tr key={index}>
                <td>{log.date}</td>
                <td>{log.clockIn ? log.clockIn.toLocaleTimeString() : '-'}</td>
                <td>{log.clockOut ? log.clockOut.toLocaleTimeString() : '-'}</td>
                <td>{calculateTotalTime(log)}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ClockInOut;