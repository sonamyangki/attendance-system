import React, { useState } from 'react';
import { Container, Typography, TextField, Button, Box, List, ListItem, ListItemText, Grid, AppBar, Toolbar, IconButton } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import SideDrawer from './sidedrawer';

function AddProject() {
  const [projectName, setProjectName] = useState('');
  const [employeeName, setEmployeeName] = useState('');
  const [employeeID, setEmployeeID] = useState('');
  const [division, setDivision] = useState('');
  const [employees, setEmployees] = useState([]);
  const [projects, setProjects] = useState([]);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = () => {
    setDrawerOpen(!drawerOpen);
  };

  const handleAddEmployee = () => {
    // Add the current employee to the employees array
    const newEmployee = {
      name: employeeName,
      id: employeeID,
      division: division
    };
    setEmployees([...employees, newEmployee]);

    // Clear the employee form fields
    setEmployeeName('');
    setEmployeeID('');
    setDivision('');
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    
    // Create a new project object with employees array
    const newProject = {
      projectName: projectName,
      employees: [...employees]
    };

    // Add the new project to the projects array
    setProjects([...projects, newProject]);

    // Reset form fields and state after submission
    setProjectName('');
    setEmployees([]);
  };

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu" onClick={toggleDrawer}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">
            Add Project
          </Typography>
        </Toolbar>
      </AppBar>
      <SideDrawer open={drawerOpen} onClose={toggleDrawer} />
      <Container maxWidth="md" style={{ marginTop: '20px' }}>
        <Typography variant="h4" gutterBottom>Add Project</Typography>
        <TextField
          label="Project Name"
          variant="outlined"
          fullWidth
          value={projectName}
          onChange={(e) => setProjectName(e.target.value)}
          style={{ marginBottom: '20px' }}
        />

        {/* Employee Details Form */}
        <Box component="form" onSubmit={handleFormSubmit}>
          <Typography variant="h6" gutterBottom>Add Employees</Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} md={4}>
              <TextField
                label="Employee Name"
                variant="outlined"
                fullWidth
                value={employeeName}
                onChange={(e) => setEmployeeName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} md={4}>
              <TextField
                label="Employee ID"
                variant="outlined"
                fullWidth
                value={employeeID}
                onChange={(e) => setEmployeeID(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} md={4}>
              <TextField
                label="Division"
                variant="outlined"
                fullWidth
                value={division}
                onChange={(e) => setDivision(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleAddEmployee}
                style={{ marginTop: '10px' }}
              >
                Add Employee
              </Button>
            </Grid>
          </Grid>

          {/* Display added employees */}
          {employees.length > 0 && (
            <Box sx={{ mt: 2 }}>
              <Typography variant="h6" gutterBottom>Added Employees:</Typography>
              <List>
                {employees.map((employee, index) => (
                  <ListItem key={index}>
                    <ListItemText
                      primary={`${employee.name} (${employee.id})`}
                      secondary={`Division: ${employee.division}`}
                    />
                  </ListItem>
                ))}
              </List>
            </Box>
          )}

          <Button type="submit" variant="contained" color="primary" style={{ marginTop: '10px' }}>
            Submit Project
          </Button>
        </Box>

        {/* Display added projects */}
        {projects.length > 0 && (
          <Box sx={{ mt: 4 }}>
            <Typography variant="h5" gutterBottom>Added Projects:</Typography>
            {projects.map((project, index) => (
              <Box key={index} sx={{ border: '1px solid #ccc', borderRadius: '4px', padding: '10px', marginBottom: '10px' }}>
                <Typography variant="h6" gutterBottom>Project: {project.projectName}</Typography>
                <List>
                  {project.employees.map((employee, empIndex) => (
                    <ListItem key={empIndex}>
                      <ListItemText
                        primary={`${employee.name} (${employee.id})`}
                        secondary={`Division: ${employee.division}`}
                      />
                    </ListItem>
                  ))}
                </List>
              </Box>
            ))}
          </Box>
        )}
      </Container>
    </div>
  );
}

export default AddProject;
