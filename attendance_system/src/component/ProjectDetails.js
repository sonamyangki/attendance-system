import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { Container, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Box } from '@mui/material';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { AppBar, Toolbar, IconButton } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import SideDrawer from './sidedrawer';
import { Link as RouterLink } from 'react-router-dom';
import '../css/ProjectDetails.css';

// Example employee data (can be replaced with API data or fetched dynamically)
const employeeData = [
  { id: 1, name: 'John Doe', timeIn: '09:00 AM', timeOut: '05:00 PM', workingDuration: '8h' },
  { id: 2, name: 'Jane Smith', timeIn: '09:30 AM', timeOut: '05:30 PM', workingDuration: '8h' },
  // Add more employee data here
];

// Example project data (can be replaced with API data or fetched dynamically)
const projectsData = [
  { id: 1, projectName: 'Project A', employees: [employeeData[0]] },
  { id: 2, projectName: 'Project B', employees: [employeeData[1]] },
  // Add more projects as needed
];

function ProjectDetails() {
  const { id } = useParams(); // Retrieve the project ID from URL parameters
  const project = projectsData.find(proj => proj.id === parseInt(id)); // Find the project by ID

  const [selectedDate, setSelectedDate] = useState(new Date());
  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = () => {
    setDrawerOpen(!drawerOpen);
  };

  if (!project) {
    return <Typography variant="h6">Project not found.</Typography>;
  }

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu" onClick={toggleDrawer}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">
            {project.projectName} Details
          </Typography>
        </Toolbar>
      </AppBar>
      <SideDrawer open={drawerOpen} onClose={toggleDrawer} />
      <Container maxWidth="md" style={{ marginTop: '20px' }}>
        <Typography variant="body1" gutterBottom>{project.details}</Typography>
        <Box sx={{ display: 'flex', justifyContent: 'flex-end', mb: 2 }}>
          <DatePicker
            selected={selectedDate}
            onChange={(date) => setSelectedDate(date)}
            dateFormat="MMMM yyyy"
            showMonthYearPicker
            className="form-control"
            wrapperClassName="date-picker-wrapper"
            style={{ width: '50px' }}
          />
        </Box>
        <Typography variant="h6" gutterBottom>
          Today's Date: {selectedDate.toLocaleDateString('en-US', { day: 'numeric', month: 'long', year: 'numeric' })}
        </Typography>
        <Typography variant="h5" gutterBottom>Employees</Typography>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell>Employee Name</TableCell>
                <TableCell>Time In</TableCell>
                <TableCell>Time Out</TableCell>
                <TableCell>Working Duration</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {project.employees.map((employee) => (
                <TableRow key={employee.id}>
                  <TableCell>{selectedDate.toLocaleDateString('en-US', { day: 'numeric', month: 'long', year: 'numeric' })}</TableCell>
                  <TableCell>{employee.name}</TableCell>
                  <TableCell>{employee.timeIn}</TableCell>
                  <TableCell>{employee.timeOut}</TableCell>
                  <TableCell>{employee.workingDuration}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
    </div>
  );
}

export default ProjectDetails;

