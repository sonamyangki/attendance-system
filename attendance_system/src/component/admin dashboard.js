import React, { useState } from 'react';
import { AppBar, Toolbar, IconButton, Typography, Container, Grid, Paper, Button } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import SideDrawer from './sidedrawer';
import { Link as RouterLink } from 'react-router-dom';

// Example employee data (can be replaced with API data or fetched dynamically)
const employeeData = [
  { id: 1, name: 'John Doe', timeIn: '09:00 AM', timeOut: '05:00 PM', workingDuration: '8h' },
  { id: 2, name: 'Jane Smith', timeIn: '09:30 AM', timeOut: '05:30 PM', workingDuration: '8h' },
  // Add more employee data here
];

// Example project data (can be replaced with API data or fetched dynamically)
const projectsData = [
  { id: 1, projectName: 'Project A', employees: [employeeData[0]] },
  { id: 2, projectName: 'Project B', employees: [employeeData[1]] },
  // Add more projects as needed
];

function Dashboard() {
  const [drawerOpen, setDrawerOpen] = useState(false);

  const toggleDrawer = () => {
    setDrawerOpen(!drawerOpen);
  };

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu" onClick={toggleDrawer}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Admin Dashboard
          </Typography>
        </Toolbar>
      </AppBar>
      <SideDrawer open={drawerOpen} onClose={toggleDrawer} />
      <Container style={{ marginTop: '20px' }}>
        <Typography variant="h4" gutterBottom>Projects</Typography>
        <Grid container spacing={3}>
          {projectsData.map((project) => (
            <Grid item xs={12} sm={6} md={4} key={project.id}>
              <Paper style={{ padding: '10px' }}>
                <Typography variant="h6">{project.projectName}</Typography>
                <Button
                  component={RouterLink}
                  to={`/project/${project.id}`}
                  variant="outlined"
                  color="primary"
                  style={{ marginTop: '10px' }}
                >
                  View Details
                </Button>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
}

export default Dashboard;
