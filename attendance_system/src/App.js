import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './component/admin login';
import Dashboard from './component/admin dashboard';
import AddProject from './component/addproject';
import ProjectDetails from './component/ProjectDetails';
import 'bootstrap/dist/css/bootstrap.min.css';
import Userpage from './component/userpage'

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/add-project" element={<AddProject />} />
          <Route path="/project/:id" element={<ProjectDetails/>} />
          <Route path="/userpage" element={<Userpage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
